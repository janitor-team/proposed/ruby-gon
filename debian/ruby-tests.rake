require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new(:spec) do |spec|
    spec.pattern      = FileList['./spec/**/*_spec.rb'] - FileList['./spec/gon/thread_spec.rb']
    spec.rspec_opts = ["--color", '--format doc', '--require spec_helper']
end

task :default => :spec
